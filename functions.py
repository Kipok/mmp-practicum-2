import numpy as np
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib as mpl
import time
from sklearn.metrics.cluster import adjusted_rand_score
import py_cluster
import cy_cluster


def measure_method(method, points, cl_num, right_clusters, cont=None, axis=None):
    clusters = np.empty(points.shape[0], dtype=np.int32)
    tm = time.time()
    if cont is None:
        method(points, points.shape[0], cl_num, clusters, min_iter=50, max_iter=50, early_stop=False)
    else:
        method(points, points.shape[0], cl_num, clusters)
    tm = time.time() - tm
    score = adjusted_rand_score(right_clusters, clusters)
    if cont is not None:
        show_map(points, clusters, cont, axis)
    return tm, score


def measure_continents(method, points, right_clusters, cont_data):
    fig, axes = plt.subplots(figsize=(18,12), ncols=3, nrows=2)
    for i, cont in enumerate(cont_data):
        tm, score = measure_method(method, points[cont_data[cont][0]],
                                   cont_data[cont][1], right_clusters[cont_data[cont][0]], cont, axes[i//3, i%3])
        axes[i//3, i%3].set_title(cont + ': ' + str(score))


def measure_continentwise(method, points, right_clusters, cont_data):
    clusters = np.empty(points.shape[0], dtype=np.int32)
    for i, cont in enumerate(cont_data):
        tmp_clusters = np.empty(points[cont_data[cont][0]].shape[0], dtype=np.int32)
        method(points[cont_data[cont][0]], points[cont_data[cont][0]].shape[0],
            cont_data[cont][1], tmp_clusters)
        clusters[cont_data[cont][0]] = tmp_clusters + 200*i

    score = adjusted_rand_score(right_clusters, clusters)
    print('Score:', score)
    show_map(points, clusters, 'World', None)


def plot_graphics(na_points, na_clusters):
    fig, axes = plt.subplots(figsize=(15,10), ncols=3, nrows=2)
    method = [[py_cluster.k_means, cy_cluster.k_means], [py_cluster.k_medoids, cy_cluster.k_medoids]]
    title = [['Python k-means', 'Cython k-means'], ['Python k-medoids', 'Cython k-medoids']]
    num = na_points.shape[0] // 100 - 1
    num_points = np.empty((2, 2, num))
    times = np.empty((2, 2, num))

    for i in range(2):
        for j in range(3):
            if j == 2:
                axes[i,j].set_title('k-means speed-up' if i == 0 else 'k-medoids speed-up')
                axes[i,j].set_xlabel('Points number')
                axes[i,j].set_ylabel('Acceleration')
                axes[i,j].plot(num_points[i,0], times[i,0] / times[i,1])
                continue

            for pt_num in range(200, na_points.shape[0], 100):
                pt_idx = np.random.choice(na_points.shape[0], pt_num, replace=False)
                tm, score = measure_method(method[i][j], na_points[pt_idx], len(set(na_clusters)), na_clusters[pt_idx])
                num_points[i, j, pt_num/100-2] = pt_num
                times[i, j, pt_num/100-2] = tm
            axes[i,j].set_title(title[i][j])
            axes[i,j].set_xlabel('Points number')
            axes[i,j].set_ylabel('Time in seconds')
            axes[i,j].plot(num_points[i,j], times[i,j])


def show_map(points, clusters, mode='NA', axis=None):
    if axis is None:
        fig = plt.figure(1, (20, 20))
    size_coef = 5000
    drawsize = True
    if mode == 'NA':
        m = Basemap(projection='ortho',lat_0=45,lon_0=-100,resolution='c', ax=axis)
        m.drawmeridians(np.arange(0,360,30))
        m.drawparallels(np.arange(-90,90,30))
    elif mode == 'AF':
        m = Basemap(projection='ortho',lat_0=0,lon_0=20,resolution='c', ax=axis)
        m.drawmeridians(np.arange(0,360,30))
        m.drawparallels(np.arange(-90,90,30))
    elif mode == 'AS':
        m = Basemap(projection='ortho',lat_0=30,lon_0=90,resolution='c', ax=axis)
        m.drawmeridians(np.arange(0,360,30))
        m.drawparallels(np.arange(-90,90,30))
    elif mode == 'EU':
        m = Basemap(projection='ortho',lat_0=60,lon_0=60,resolution='c', ax=axis)
        m.drawmeridians(np.arange(0,360,30))
        m.drawparallels(np.arange(-90,90,30))
    elif mode == 'OC':
        m = Basemap(projection='ortho',lat_0=-30,lon_0=140,resolution='c', ax=axis)
        m.drawmeridians(np.arange(0,360,30))
        m.drawparallels(np.arange(-90,90,30))
    elif mode == 'SA':
        m = Basemap(projection='ortho',lat_0=-10,lon_0=-60,resolution='c', ax=axis)
        m.drawmeridians(np.arange(0,360,30))
        m.drawparallels(np.arange(-90,90,30))
    elif mode == 'World':
        m = Basemap(projection='mill',llcrnrlat=-60,urcrnrlat=90, llcrnrlon=-180,urcrnrlon=180,resolution='c')
        drawsize=False
    else:
        print('Wrong mode..')
        return
    m.drawmapboundary(fill_color='aqua')
    m.fillcontinents(color='coral',lake_color='aqua', zorder=0)
    m.drawcoastlines()
    m.drawcountries()

    num_cl = len(set(clusters))
    colors = np.linspace(0, 1, num_cl)

    norm = mpl.colors.Normalize(vmin=0, vmax=1)
    cmap = cm.hot
    mp = cm.ScalarMappable(norm=norm, cmap='nipy_spectral')

    mx = np.max(points[:,0])

    for cl, c in zip(set(clusters), colors):
        tmp_p = points[clusters == cl]
        if drawsize is True:
            # m.scatter(*m(tmp_p[:,1],tmp_p[:,2]),edgecolors='black',
            #           facecolor=mp.to_rgba(c),s=list(np.minimum(np.maximum(tmp_p[:,0]/mx*size_coef, 20), 2500)))
            m.scatter(*m(tmp_p[:,1],tmp_p[:,2]),color=mp.to_rgba(c))
        else:
            # m.scatter(*m(tmp_p[:,1],tmp_p[:,2]),edgecolors='black',facecolor=mp.to_rgba(c))
            m.scatter(*m(tmp_p[:,1],tmp_p[:,2]),color=mp.to_rgba(c))
