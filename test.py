import numpy as np
import cy_cluster
import py_cluster

random_data = (np.random.rand(100, 3) * 100).astype(np.float32)
cl_num = 10
clusters = np.random.randint(1, 11, size=100).astype(np.int32)

py_cluster.k_means(random_data, random_data.shape[0], cl_num, clusters)
py_cluster.k_medoids(random_data, random_data.shape[0], cl_num, clusters)
cy_cluster.k_means(random_data, random_data.shape[0], cl_num, clusters)
cy_cluster.k_medoids(random_data, random_data.shape[0], cl_num, clusters)
