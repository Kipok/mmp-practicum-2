#include "k_medoids.h"
#include <vector>
#include <cmath>
#include <algorithm>
#include <set>

const float pi = 3.141592653589793;

#define dst(i, j) acos(cx[i].lat*cx[j].lat*(cx[i].lon*cx[j].lon+sx[i].lon*sx[j].lon)+sx[i].lat*sx[j].lat)

struct Point {
    float lat;
    float lon;
};

void k_medoids_cpp(float *points, int p_num, int cl_num, int *clusters, int min_iter, int max_iter, int early_stop)
{
    std::vector <float> medoids(cl_num);
    std::vector <std::vector <float> > whole_dst(p_num);
    std::vector <bool> flags(cl_num, true);
    std::set <int> cl_idx[cl_num];

    for (int i = 0; i < p_num; i++) {
        whole_dst[i].resize(p_num, -1);
        clusters[i] = i % cl_num;
    }
    std::random_shuffle(clusters, clusters + p_num);
    for (int i = 0; i < p_num; i++) {
        cl_idx[clusters[i]].insert(i);
    }

    std::vector<Point> cx(p_num), sx(p_num);
    for (int i = 0; i < p_num; i++) {
        cx[i].lon = cos(points[i * 3 + 1] * pi / 180);
        cx[i].lat = cos(points[i * 3 + 2] * pi / 180);
        sx[i].lon = sin(points[i * 3 + 1] * pi / 180);
        sx[i].lat = sin(points[i * 3 + 2] * pi / 180);
    }

    int t = 0;
    bool flag = true;
    while (t < min_iter || flag) {
        flag = false;
        ++t;
        if (t > max_iter) {
            break;
        }

        for (int cl = 0; cl < cl_num; cl++) {
            if (flags[cl] == false) {
                continue;
            }
            float best_dist = 1e9, cur_dist = 0;
            int best_pos = -1;
            for (std::set<int>::iterator id = cl_idx[cl].begin(); id != cl_idx[cl].end(); ++id) {
                cur_dist = 0;
                for (std::set<int>::iterator n_id = cl_idx[cl].begin(); n_id != cl_idx[cl].end(); ++n_id) {
                    if (whole_dst[*id][*n_id] == -1) {
                        whole_dst[*id][*n_id] = dst(*id, *n_id);
                    }
                    cur_dist += whole_dst[*id][*n_id] * points[(*n_id) * 3];
                }
                if (cur_dist < best_dist) {
                    best_dist = cur_dist;
                    best_pos = *id;
                }
            }
            medoids[cl] = best_pos;
        }
        int num_changed = 0;
        for (int m = 0; m < cl_num; m++) {
            if (flags[m] == false) {
                continue;
            }
            if (early_stop) {
                flags[m] = false;
            }
            for (int p = 0; p < p_num; p++) {
                if (whole_dst[p][medoids[m]] == -1) {
                    whole_dst[p][medoids[m]] = dst(p, medoids[m]);
                }
                if (whole_dst[p][medoids[clusters[p]]] == -1) {
                    whole_dst[p][medoids[clusters[p]]] = dst(p, medoids[clusters[p]]);
                }
                if (whole_dst[p][medoids[m]] < whole_dst[p][medoids[clusters[p]]]) {
                    cl_idx[clusters[p]].erase(p);
                    flags[clusters[p]] = flags[m] = true;
                    flag = true;
                    clusters[p] = m;
                    cl_idx[clusters[p]].insert(p);
                }
            }
            num_changed += flags[m];
        }
        if (early_stop and num_changed <= cl_num * 1.0) {
            break;
        }
    }
}
