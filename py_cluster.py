import numpy as np


def distances(x, y):
    xr = x * np.pi / 180
    yr = y * np.pi / 180
    cx, cy = np.cos(xr[:,2]), np.cos(yr[:,2])
    sx, sy = np.sin(xr[:,2]), np.sin(yr[:,2])

    res = np.dot(cx[:,np.newaxis], cy.T[np.newaxis,:])
    res *= np.cos(xr[:,1][:,np.newaxis] - yr[:,1][np.newaxis,:])
    res += np.dot(sx[:,np.newaxis], sy.T[np.newaxis,:])
    res[res > 1.0] = 1.0
    res[res < -1.0] = -1.0

    return np.arccos(res)

def to_sphere(p):
    phi = np.arcsin(p[2])
    l = np.arctan2(p[1], p[0])
    return np.array([l, phi], dtype=np.float32)


def segment_center(p1, p2, rad2, crad2, srad2):
    rad1 = p1[1:]*np.pi/180
    crad1 = np.cos(rad1)
    srad1 = np.sin(rad1)
    v1 = np.array([crad1[1]*crad1[0], crad1[1]*srad1[0], srad1[1]], dtype=np.float32)
    v2 = np.array([crad2[1]*crad2[0], crad2[1]*srad2[0], srad2[1]], dtype=np.float32)
    if np.abs(v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2] - 1) < 1e-5:
        return np.array([p1[0] + p2[0], p1[1], p1[2]], dtype=np.float32)
    alpha = np.arccos(np.dot(v1, v2))
    beta = alpha * p2[0] / (p1[0] + p2[0])
    axis = np.cross(v1, v2)
    axis /= np.linalg.norm(axis)
    cb = np.cos(beta)
    sb = np.sin(beta)
    xx = axis[0]**2
    yy = axis[1]**2
    zz = axis[2]**2
    xy = axis[0]*axis[1]
    xz = axis[0]*axis[2]
    yz = axis[1]*axis[2]
    m = np.array([
                    [cb + (1-cb)*xx, (1-cb)*xy - sb*axis[2], (1-cb)*xz + sb*axis[1]],
                    [(1-cb)*xy + sb*axis[2], cb + (1-cb)*yy, (1-cb)*yz - sb*axis[0]],
                    [(1-cb)*xz - sb*axis[1], (1-cb)*yz + sb*axis[0], cb + (1-cb)*zz]
                ], dtype=np.float32)
    p = np.empty(p1.shape, dtype=np.float32)
    p[1:] = to_sphere(m.dot(v1)) * 180 / np.pi
    p[0] = p1[0] + p2[0]
    return p


def mass_center(points):
    rad_points = points[:,1:] * np.pi / 180.0
    cpoints = np.cos(rad_points)
    spoints = np.sin(rad_points)

    m_center = points[0]
    for i in range(1, points.shape[0]):
        m_center = segment_center(m_center, points[i], rad_points[i], cpoints[i], spoints[i])
    return m_center

def k_means(points, p_num, cl_num, clusters, min_iter=0, max_iter=50, early_stop=True):
    new_clusters = np.empty(p_num, dtype=np.int)
    centers = np.ones((cl_num, 3), dtype=np.float32)
    centers[:,1] = np.random.uniform(np.min(points[:,1]), np.max(points[:,1]), cl_num)
    centers[:,2] = np.random.uniform(np.min(points[:,2]), np.max(points[:,2]), cl_num)

    i = 0
    while i < min_iter or not (new_clusters == clusters).all():
        i = i + 1
        if i > max_iter:
            break
        clusters[:] = new_clusters

            # E-step
        dst = distances(points, centers)
        new_clusters = np.argmin(dst, axis=1)
            # M-step
        num_changed = 0
        for cl in range(cl_num):
            tmp = points[new_clusters == cl]
            if tmp.shape[0] != 0 and (not early_stop or ((new_clusters == cl) != (clusters == cl)).any()):
                num_changed += 1
                centers[cl] = mass_center(tmp)
        if early_stop and num_changed < cl_num * 0.6:
            break


def k_medoids(points, p_num, cl_num, clusters, min_iter=0, max_iter=50, early_stop=False):
    new_clusters = np.random.permutation(np.hstack((np.random.choice(cl_num, p_num - cl_num), np.arange(cl_num))))
    medoids = np.empty(cl_num, dtype=np.int)

    i = 0
    whole_dst = distances(points, points)
    while i < min_iter or not (new_clusters == clusters).all():
        # I'm starting from M-step because it leads to more
        # uniform distribution on medoids and thus greater quality

        i = i + 1
        if i > max_iter:
            break
            # M-step
        num_changed = 0
        for cl in range(cl_num):
            if early_stop or ((new_clusters == cl) == (clusters == cl)).all():
                continue
            cl_idx = np.nonzero(new_clusters == cl)[0]
            weights = points[cl_idx, 0]
            best_dist = 1e9
            best_pos = -1
            for id in cl_idx:
                cur_dist = np.sum(whole_dst[cl_idx, id] * weights)
                if cur_dist < best_dist:
                    best_dist = cur_dist
                    best_pos = id
            if medoids[cl] != best_pos:
                medoids[cl] = best_pos
                num_changed += 1
            # E-step
        new_clusters = np.argmin(whole_dst[:,medoids], axis=1)

        clusters[:] = new_clusters
        if early_stop and num_changed <= cl_num * 1.0:
            break
