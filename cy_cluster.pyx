cimport cython
import cython
cimport numpy as np
import numpy as np
from libc.math cimport sin, cos, sqrt, acos, asin, atan2
from libcpp.vector cimport vector

cdef extern from 'k_medoids.h':
    void k_medoids_cpp(float *points, int p_num, int cl_num, int *clusters, int min_iter, int max_iter, int early_stop)

ctypedef float POINTS_TYPE_T
ctypedef int  CLUSTERS_TYPE_T

cdef POINTS_TYPE_T pi = 3.141592653589793

cdef struct Point:
    POINTS_TYPE_T weight
    POINTS_TYPE_T lon
    POINTS_TYPE_T lat

cdef struct NoWeightPoint:
    POINTS_TYPE_T lon
    POINTS_TYPE_T lat

cdef inline POINTS_TYPE_T fast_min(POINTS_TYPE_T a, POINTS_TYPE_T b): return a if a < b else b

cdef inline POINTS_TYPE_T fast_max(POINTS_TYPE_T a, POINTS_TYPE_T b): return a if a > b else b

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef inline distances(vector[Point] &y,
              vector[NoWeightPoint] &cx,
              vector[NoWeightPoint] &cy,
              vector[NoWeightPoint] &sx,
              vector[NoWeightPoint] &sy,
              vector[vector[POINTS_TYPE_T]] &res,
              vector[int] &flags
              ):
    cdef Py_ssize_t i, j
    cdef POINTS_TYPE_T acos_arg

    for i in range(y.size()):
        if flags[i] == 0:
            continue
        cy[i].lon = cos(y[i].lon*pi/180)
        cy[i].lat = cos(y[i].lat*pi/180)
        sy[i].lon = sin(y[i].lon*pi/180)
        sy[i].lat = sin(y[i].lat*pi/180)

    for j in range(cy.size()):
        if flags[j] == 0:
            continue
        for i in range(cx.size()):
            res[i][j] = acos(cx[i].lat*cy[j].lat*(cx[i].lon*cy[j].lon+sx[i].lon*sy[j].lon)+sx[i].lat*sy[j].lat)

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef inline Point segment_center(Point &p1, Point &p2):
    cdef POINTS_TYPE_T rad10 = p1.lon*pi/180
    cdef POINTS_TYPE_T rad11 = p1.lat*pi/180
    cdef POINTS_TYPE_T crad10 = cos(rad10)
    cdef POINTS_TYPE_T crad11 = cos(rad11)
    cdef POINTS_TYPE_T srad10 = sin(rad10)
    cdef POINTS_TYPE_T srad11 = sin(rad11)

    cdef POINTS_TYPE_T rad20 = p2.lon*pi/180
    cdef POINTS_TYPE_T rad21 = p2.lat*pi/180
    cdef POINTS_TYPE_T crad20 = cos(rad20)
    cdef POINTS_TYPE_T crad21 = cos(rad21)
    cdef POINTS_TYPE_T srad20 = sin(rad20)
    cdef POINTS_TYPE_T srad21 = sin(rad21)


    cdef POINTS_TYPE_T v10 = crad11*crad10
    cdef POINTS_TYPE_T v11 = crad11*srad10
    cdef POINTS_TYPE_T v12 = srad11
    cdef POINTS_TYPE_T v20 = crad21*crad20
    cdef POINTS_TYPE_T v21 = crad21*srad20
    cdef POINTS_TYPE_T v22 = srad21

    cdef Point p

    if abs(v10 * v20 + v11 * v21 + v12 * v22 - 1) < 1e-5:
        p.weight = p1.weight + p2.weight
        p.lon = p1.lon
        p.lat = p1.lat
        return p

    cdef POINTS_TYPE_T alpha = acos(v10 * v20 + v11 * v21 + v12 * v22)
    cdef POINTS_TYPE_T beta = alpha * p2.weight / (p1.weight + p2.weight)
    cdef POINTS_TYPE_T axis0 = v11*v22-v12*v21
    cdef POINTS_TYPE_T axis1 = v12*v20-v10*v22
    cdef POINTS_TYPE_T axis2 = v10*v21-v11*v20
    cdef POINTS_TYPE_T norm = sqrt(axis0*axis0+axis1*axis1+axis2*axis2)
    axis0 /= norm
    axis1 /= norm
    axis2 /= norm

    cdef POINTS_TYPE_T cb = cos(beta)
    cdef POINTS_TYPE_T sb = sin(beta)
    cdef POINTS_TYPE_T xx = axis0*axis0
    cdef POINTS_TYPE_T yy = axis1*axis1
    cdef POINTS_TYPE_T zz = axis2*axis2
    cdef POINTS_TYPE_T xy = axis0*axis1
    cdef POINTS_TYPE_T xz = axis0*axis2
    cdef POINTS_TYPE_T yz = axis1*axis2
    cdef POINTS_TYPE_T m00 = cb + (1-cb)*xx
    cdef POINTS_TYPE_T m01 = (1-cb)*xy - sb*axis2
    cdef POINTS_TYPE_T m02 = (1-cb)*xz + sb*axis1
    cdef POINTS_TYPE_T m10 = (1-cb)*xy + sb*axis2
    cdef POINTS_TYPE_T m11 = cb + (1-cb)*yy
    cdef POINTS_TYPE_T m12 = (1-cb)*yz - sb*axis0
    cdef POINTS_TYPE_T m20 = (1-cb)*xz - sb*axis1
    cdef POINTS_TYPE_T m21 = (1-cb)*yz + sb*axis0
    cdef POINTS_TYPE_T m22 = cb + (1-cb)*zz

    p.weight = p1.weight + p2.weight
    p.lon = atan2(m10*v10+m11*v11+m12*v12, m00*v10+m01*v11+m02*v12) * 180 / pi
    p.lat = asin(m20*v10+m21*v11+m22*v12) * 180 / pi
    return p

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef inline Point mass_center(vector[Point] &points, vector[int] &clusters, int cl, Point &old_cl):
    cdef Point m_center
    cdef Py_ssize_t i

    m_center.weight = -1

    for i in range(points.size()):
        if clusters[i] != cl:
            continue
        if m_center.weight == -1:
            m_center = points[i]
            continue
        m_center = segment_center(m_center, points[i])
    if m_center.weight == -1:
        return old_cl
    return m_center


@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def k_means(np.ndarray[float, ndim=2, mode='c'] np_points not None,
            int p_num,
            int cl_num,
            np.ndarray[int, ndim=1, mode='c'] clusters not None,
            int min_iter=0,
            int max_iter=50,
            int early_stop=True):
    cdef vector[CLUSTERS_TYPE_T] new_clusters
    cdef vector[Point] centers
    cdef vector[Point] points
    cdef vector[NoWeightPoint] cx, cy, sx, sy
    cdef vector[vector[POINTS_TYPE_T]] dst
    cdef vector[int] flags
    cdef Py_ssize_t i = 0, j, k, cl
    cdef int flag = 1
    cdef POINTS_TYPE_T min_lon = 180.0, max_lon = -180.0, min_lat = 90.0, max_lat = -90.0
    cdef int num_changed = 0

    new_clusters.resize(p_num)
    centers.resize(cl_num)
    cx.resize(p_num)
    sx.resize(p_num)
    cy.resize(cl_num)
    sy.resize(cl_num)
    dst.resize(p_num)
    points.resize(p_num)
    flags.resize(cl_num)

    for j in range(p_num):
        dst[j].resize(cl_num)
        cx[j].lon = cos(np_points[j,1]*pi/180)
        cx[j].lat = cos(np_points[j,2]*pi/180)
        sx[j].lon = sin(np_points[j,1]*pi/180)
        sx[j].lat = sin(np_points[j,2]*pi/180)
        points[j].weight = np_points[j,0]
        points[j].lon = np_points[j,1]
        points[j].lat = np_points[j,2]
        min_lon = fast_min(min_lon, points[j].lon)
        max_lon = fast_max(max_lon, points[j].lon)
        min_lat = fast_min(min_lat, points[j].lat)
        max_lat = fast_max(max_lat, points[j].lat)
        new_clusters[j] = 0

    for j in range(cl_num):
        centers[j].lon = np.random.uniform(min_lon, max_lon)
        centers[j].lat = np.random.uniform(min_lat, max_lat)
        centers[j].weight = 1
        flags[j] = 1

    while i < min_iter or flag:
        flag = 0
        i = i + 1
        if i > max_iter:
            break
        distances(centers, cx, cy, sx, sy, dst, flags)
        for j in range(p_num):
            for k in range(cl_num):
                if dst[j][new_clusters[j]] > dst[j][k]:
                    flags[new_clusters[j]] = 1
                    flags[k] = 1
                    flag = 1
                    new_clusters[j] = k
        num_changed = 0
        for cl in range(cl_num):
            if flags[cl] == 1:
                centers[cl] = mass_center(points, new_clusters, cl, centers[cl])
                if early_stop:
                    flags[cl] = 0
                num_changed += 1
        if early_stop and num_changed <= cl_num * 0.6:
            break

    for j in range(p_num):
        clusters[j] = new_clusters[j]

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
def k_medoids(np.ndarray[float, ndim=2, mode='c'] np_points not None,
            int p_num,
            int cl_num,
            np.ndarray[int, ndim=1, mode='c'] clusters not None,
            int min_iter=0,
            int max_iter=50,
            int early_stop=True):
    k_medoids_cpp(&np_points[0,0], p_num, cl_num, &clusters[0], min_iter, max_iter, early_stop)
